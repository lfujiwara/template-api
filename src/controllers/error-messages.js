module.exports = {
  PASSWORD_CONFIRMATION_DID_NOT_MATCH: "Password confirmation did not match",
  UNKNOWN_ERROR: "Unknown error",
};
