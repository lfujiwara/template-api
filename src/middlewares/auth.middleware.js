const jwt = require("jsonwebtoken");
const isString = require("../util/is-string");

const authMiddlwareFactory = ({ secret }) => (req, res, next) => {
  const authHeader = req.headers.authentication;
  const token = authHeader && authHeader.toString().split(" ")[1];

  if (!authHeader || !isString(token) || !token) {
    res.status(401).json({ error: "Access Token not found" });
    return;
  }

  try {
    req.user = jwt.verify(token, secret).id;
    next();
  } catch {
    res.status(401).json({ error: "Access Token not found" });
    return;
  }
};

module.exports = authMiddlwareFactory;
