const makeChain = (...validators) => (obj) => {
  for (let i = 0; i < validators.length; i++) {
    const validator = validators[i];
    const err = validator(obj);
    if (err) return err;
  }
  return undefined;
};

module.exports = { makeChain };
