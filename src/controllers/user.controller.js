const { Router } = require("express");
const { createUserRequestValidator } = require("./user.controller.validator");
const { handleErrorsToHttp } = require("./user.controller.errors");
const { User } = require("../models/user");
const { v4: uuid } = require("uuid");
const authMiddlwareFactory = require("../middlewares/auth.middleware");
const { topics, makeEventMessage } = require("../util/topics");

const userControllerFactory = ({ stanConn, mongoClient, secret }) => {
  const userController = Router();
  const authMiddlware = authMiddlwareFactory({ secret });
  const usersCollection = mongoClient.db("local").collection("users");

  function handleErr(validationErr, res) {
    const errData = handleErrorsToHttp(validationErr);
    res.status(errData.status).json(errData.body);
  }

  userController.post("/", async (req, res) => {
    const validationErr = createUserRequestValidator(req);
    if (validationErr) {
      handleErr(validationErr, res);
      return;
    }

    const form = req.body;
    const userOrError = User.create(
      uuid(),
      form.name,
      form.email,
      form.password
    );
    if (!userOrError.ok) {
      handleErr(new Error(userOrError.value && userOrError.value.message));
      return;
    }

    const user = userOrError.value;

    if (await usersCollection.findOne({ email: user.email })) {
      res.status(409).json({ error: "User with email already exists" });
      return;
    }

    stanConn.publish(
      topics.UserCreated,
      makeEventMessage(topics.UserCreated, user.id, {
        name: user.name,
        email: user.email,
        password: user.password,
      })
    );

    res.status(201).json({
      user: {
        id: user.id,
        name: user.name,
        email: user.email,
      },
    });
  });

  userController.delete("/:id", authMiddlware, async (req, res) => {
    if (req.user !== req.params.id) {
      res.status(403).json({ error: "Access Token did not match User ID" });
      return;
    }

    if (!(await usersCollection.findOne({ id: req.user }))) {
      res.status(404).json({ error: "User with email does not exist" });
      return;
    }

    stanConn.publish(
      topics.UserDeleted,
      makeEventMessage(topics.UserDeleted, req.user, {})
    );
    res.status(200).json({ id: req.user });
  });

  return userController;
};

module.exports = userControllerFactory;
