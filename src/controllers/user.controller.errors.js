const errorMessages = require("./error-messages");

class MissingFieldError extends Error {
  constructor(field) {
    super();
    this.name = "MissingFieldError";
    this.field = field;
  }

  handleHttp() {
    return {
      status: 400,
      body: {
        error: `Request body had missing field ${this.field}`,
      },
    };
  }
}

class MalformedFieldError extends Error {
  constructor(field) {
    super();
    this.name = "MalformedFieldError";
    this.field = field;
  }

  handleHttp() {
    return {
      status: 400,
      body: {
        error: `Request body had malformed field ${this.field}`,
      },
    };
  }
}

class InvalidFieldError extends Error {
  constructor(field) {
    super();
    this.name = "InvalidFieldError";
    this.field = field;
  }

  handleHttp() {
    return {
      status: 400,
      body: {
        error: `Request body had invalid field ${this.field}`,
      },
    };
  }
}

class SemanticError extends Error {
  constructor(errorCode) {
    super();
    this.name = "SemanticError";
    this.errorCode = errorCode;
  }

  handleHttp() {
    return {
      status: 422,
      body: {
        error: errorMessages[this.errorCode] || errorMessages.UNKNOWN_ERROR,
      },
    };
  }
}

const errorCodes = {
  PASSWORD_CONFIRMATION_DID_NOT_MATCH: "PASSWORD_CONFIRMATION_DID_NOT_MATCH",
};

const requestFields = {
  name: "name",
  email: "email",
  password: "password",
  passwordConfirmation: "passwordConfirmation",
};

const handleErrorsToHttp = (err) => {
  if (
    err instanceof MissingFieldError ||
    err instanceof MalformedFieldError ||
    err instanceof SemanticError ||
    err instanceof InvalidFieldError
  ) {
    return err.handleHttp();
  }

  return {
    status: 500,
    body: {
      error: err.message || errorMessages.UNKNOWN_ERROR,
    },
  };
};

module.exports = {
  MissingFieldError,
  MalformedFieldError,
  InvalidFieldError,
  SemanticError,
  handleErrorsToHttp,
  errorCodes,
  requestFields,
};
