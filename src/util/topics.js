const topics = {
  UserCreated: "UserCreated",
  UserDeleted: "UserDeleted",
};

const makeEventMessage = (eventType, entityId, entityAggregate) =>
  JSON.stringify({
    eventType,
    entityId,
    entityAggregate,
  });

module.exports = { topics, makeEventMessage };
