const {
  MalformedFieldError,
  SemanticError,
  MissingFieldError,
  errorCodes,
  requestFields,
  InvalidFieldError,
} = require("./user.controller.errors");
const isString = require("../util/is-string");
const { makeChain } = require("../util/chain-validator");

const existsAndIsString = (field) => (x) => {
  if (x[field] === undefined) return new MissingFieldError(field);
  if (!isString(x[field])) return new MalformedFieldError(field);
};

const passwordVerification = (x) => {
  if (x.password !== x.passwordConfirmation)
    return new SemanticError(errorCodes.PASSWORD_CONFIRMATION_DID_NOT_MATCH);
};

const isEmail = (field) => (x) => {
  const email = x[field];
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!re.test(String(email).toLowerCase())) {
    return new InvalidFieldError(field);
  }
};

const isLengthInRange = (field, min, max) => (x) => {
  if (x[field].length < min || x[field].length > max)
    return new InvalidFieldError(field);
};

const isAlphanumeric = (field) => (x) => {
  const re = /^[0-9A-Za-z]+$/;
  if (!re.test(x[field])) return new InvalidFieldError(field);
};

const createUserRequestValidator = ({ body }) =>
  makeChain(
    existsAndIsString(requestFields.name),
    isLengthInRange(requestFields.name, 1, Infinity),
    existsAndIsString(requestFields.email),
    isEmail(requestFields.email),
    existsAndIsString(requestFields.password),
    existsAndIsString(requestFields.passwordConfirmation),
    passwordVerification,
    isLengthInRange(requestFields.password, 8, 32),
    isAlphanumeric(requestFields.password)
  )(body);

module.exports = { createUserRequestValidator };
