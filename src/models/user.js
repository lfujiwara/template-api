const Result = require("../util/result");

class User {
  constructor(id, name, email, password) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.password = password;
  }

  static create(id, name, email, password) {
    return Result.ok(new User(id, name, email, password));
  }

  toDocument() {
    return {
      id: this.id,
      name: this.name,
      email: this.email,
      password: this.password,
    };
  }

  static fromDocument(d) {
    return new User(d.id, d.name, d.email, d.password);
  }
}

module.exports = { User };
