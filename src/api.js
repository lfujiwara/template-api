const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const userController = require("./controllers/user.controller");
const userControllerFactory = require("./controllers/user.controller");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */
  const userController = userControllerFactory({
    stanConn,
    mongoClient,
    secret,
  });
  api.use("/users", userController);

  return api;
};
